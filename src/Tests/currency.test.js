import Currency from '../Model/Currency';

describe('Currency Test', () => {
    const input=2;
    const rate=10000;    
    const currency = new Currency("IDR",{IDR:rate});

    test('Currency Name : IDR for Indonesian Rupiah',()=>{
        expect(currency.currName).toBe('Indonesian Rupiah');
    })

    test('Total Rate : input * rate',()=>{
        expect(currency.getTotal(input)).toBe(input*rate);
    })

});

