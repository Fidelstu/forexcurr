import React, { Component } from "react";
import AddBtnControl from './AddBtnControl';
import SubmitBtnControl from './SubmitBtnControl';
import Currencies from '../Model/Currencies';
import  {Row} from 'react-bootstrap';
  
class AddCurrency extends Component {
  constructor(props) {
    super(props);

    this.state = { 
        inputCurr: "" 
        ,isAdd : false
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmitBtn = this.onSubmitBtn.bind(this);
    this.onVisibleAddCurrency = this.onVisibleAddCurrency.bind(this);

  }

  onVisibleAddCurrency(){
    this.setState({ isAdd:true });
  }

  onInputChange(inputCurr) {
    inputCurr = inputCurr.toUpperCase();
      
    this.setState({ inputCurr });
  }  

  onSubmitBtn(inputCurr){
    if(Currencies[inputCurr]) this.props.onAddCurr(inputCurr);
    
    this.setState({ isAdd:false });
  }

  render() {
    return (
        <Row className="AddCurr">
            {
                !this.state.isAdd ?
                    <AddBtnControl 
                        onVisibleAddCurrency = {this.onVisibleAddCurrency}
                    />
                :
                    <SubmitBtnControl 
                        inputCurr = {this.state.inputCurr}
                        onInputChange = {this.onInputChange}
                        onSubmitBtn = {this.onSubmitBtn}
                    />    
            }
                    
        </Row>
    );
  }

}

export default AddCurrency;
