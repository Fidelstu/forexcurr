import React, { Component } from "react";
import  { Col, Row, FormControl, ControlLabel, Form,FormGroup } from 'react-bootstrap';
  
class HeadCurr extends Component {
  constructor(props) {
    super(props);

    this.state = { inputRate: "1.00" };

  }

  onInputChange(inputRate) {
    this.setState({ inputRate });
    this.props.onInputChange(inputRate);
  }  

  render() {
    return (
        <Row className="HeadCurr">
            <Col xs={12} align="left">
            <ControlLabel className="Detail">United States Dollars</ControlLabel>  
            </Col>
            <Col xs={12} align="left">
                <Form horizontal>
                    <FormGroup>
                    <Col xs={6}>
                        <ControlLabel>USD</ControlLabel>    
                    </Col>  
                    <Col xs={6}>                 
                        <FormControl
                        className="right"
                        type="text"
                        placeholder="Enter rates"  
                        name = "inputRate"

                        value = {this.state.inputRate} 
                        onChange={event => this.onInputChange(event.target.value)}             
                        />
                    </Col>  
                    </FormGroup>
                </Form>
            </Col>
            </Row>
    );
  }

}

export default HeadCurr;
