import React from "react";
import CurrencyItem from './CurrencyItem';
import Currency from '../Model/Currency';
import  { Col, Row } from 'react-bootstrap';
  
const ListCurrency= ({rates,inputRate,listCurrency, onRemoveBtn}) =>{
  const currencyItems = listCurrency.map((curr,i)=>{
    const currency = new Currency(curr,rates);
     
    return(
      <CurrencyItem 
        input= {inputRate}
        currency = {currency}
        key={i}         
        onRemoveBtn={onRemoveBtn}
      />
    )
  });  

  return(
    <Row>
      <Col xs={12}>
        <ul>
          {currencyItems}          
        </ul>
      </Col>
    </Row>
  )
}

export default ListCurrency;
