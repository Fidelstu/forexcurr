import React from "react";
import  { Col,Button,Glyphicon } from 'react-bootstrap';

const CurrencyItem= ({input, currency, onRemoveBtn}) =>{
    const {curr,currName,rate} = currency;
    
    return(
        <li  className="listItem" align="left">
            <Col xs={9} className="contentCurr">
                <Col xs={2}>{curr}</Col>  <Col xs={6} xsOffset={3} className="right">{currency.getTotal(input)}</Col>
                <Col xs={12} className="Detail">{currName}</Col>
                <Col xs={12} className="oneRate"> 1 USD = {curr} {rate}</Col>
            </Col> 

            <Col xs={3} className="actionContent" align="right">
                <Button bsStyle="danger" block curr={curr} onClick={ () => onRemoveBtn(curr)}> 
                <Glyphicon glyph="minus " />
                </Button>
            </Col>
        </li>
    )
   
}



export default CurrencyItem;