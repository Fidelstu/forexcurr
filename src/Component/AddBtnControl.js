import React from "react";
import  { Col,Button,Glyphicon} from 'react-bootstrap';
  
const AddBtnControl = ({onVisibleAddCurrency})=>{
    return(
        <Col xs={12}>
            <Button bsStyle="info" block onClick={onVisibleAddCurrency}>
            <Glyphicon glyph="plus pull-left" />
            Add More Currencies</Button>
        </Col>
    )
}

export default AddBtnControl;