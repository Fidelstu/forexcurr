import React from "react";
import  { Col, FormControl, Button, Form, InputGroup} from 'react-bootstrap';
  
const SubmitBtnControl = ({inputCurr, onInputChange,onSubmitBtn}) =>{    
    return(
        <Col xs={12}>
            <Form horizontal>
                <InputGroup>       

                    <FormControl type="text" 
                        name ="inputCurr"
                        value = {inputCurr} 
                        onChange={event => onInputChange(event.target.value)}                          
                    />
                    <InputGroup.Button>
                    <Button bsStyle="info" onClick={()=>onSubmitBtn(inputCurr)}>Submit</Button>
                    </InputGroup.Button>
                </InputGroup>
            </Form>
        </Col>
    )
}

export default SubmitBtnControl;