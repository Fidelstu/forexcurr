import React, { Component } from 'react';
import HeadCurr from './Component/HeadCurr';
import ListCurrency from './Component/ListCurrency';
import AddCurrency from './Component/AddCurrency';
import './App.css';
import axios from 'axios';
import  { Col } from 'react-bootstrap';

class App extends Component {
  constructor(props){
    super(props);

    this.timeUpdate = 1000 * 5; //ms * s * m * h

    this.state = {
      rates : {}
      ,inputRate : "1.00"
      ,listCurrency:[
        "IDR","EUR","GBP","SGD"
      ]
    }
    
    this.fetchData();
    
    this.onInputChange = this.onInputChange.bind(this);    
    this.onRemoveBtn = this.onRemoveBtn.bind(this);
    this.onAddCurr = this.onAddCurr.bind(this);
    this.fetchData = this.fetchData.bind(this);
  }   

  componentDidMount(){
    setInterval(this.fetchData, this.timeUpdate);
  }
  
  async fetchData(){
    const {data} = await axios.get('https://api.exchangeratesapi.io/latest?base=USD');
    
    const prevRates = this.state.rates;
    const rates = data.rates;
    
    if (JSON.stringify(rates) !== JSON.stringify(prevRates)) {
      this.setState({rates});   
    } 
  }

  onInputChange(inputRate) {
    this.setState({inputRate}); 
  }

  onRemoveBtn(curr){
    let listCurrency= this.state.listCurrency;
    listCurrency.splice(listCurrency.indexOf(curr),1);
    
    this.setState({listCurrency});    
  }

  onAddCurr(curr){
    let listCurrency= this.state.listCurrency;
    
    if(listCurrency.indexOf(curr) === -1) listCurrency.push(curr);
    
    this.setState({listCurrency});    
  }

  render() {
    return (
      <div className="App">
        
        <Col className="currency" xs={10} xsPush={1}>

          <HeadCurr onInputChange={this.onInputChange}/>          

          <ListCurrency 
            rates={this.state.rates}
            inputRate={this.state.inputRate}
            listCurrency={this.state.listCurrency}   
            onRemoveBtn={this.onRemoveBtn}  
          />       
              
          <AddCurrency 
            onAddCurr = {this.onAddCurr}
          />

        </Col>
      </div>
      
    );
  }
}

export default App;
