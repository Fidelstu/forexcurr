import Currencies from './Currencies';

export default class Currency{
    constructor(curr,rates){
        this.curr = curr;
        this.rate = rates[curr]?this.getFixed(rates[curr]) : "";
        
        this.currName = Currencies[curr] ? Currencies[curr]["name"] : "";
    }

    getFixed(number){
        return number.toFixed(4) * 1;
    }

    getTotal(inputRate){
        const input = isNaN(inputRate*1)?0:inputRate;
        const rate =  isNaN(this.rate*1)?0:this.rate;
        return this.getFixed(input * rate);
    }
}