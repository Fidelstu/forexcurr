const Currencies={
    IDR:{
        name:"Indonesian Rupiah"
    }
    ,EUR:{
        name:"Euro"
    }
    ,GBP:{
        name:"British Pound"
    }
    ,SGD:{
        name:"Singapore Dollar"
    }
    ,JPY:{
        name:"Japanese Yen"
    }
}

export default Currencies;