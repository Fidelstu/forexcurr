## How to Usage

### Clone / Download
    git clone https://gitlab.com/Fidelstu/forexcurr
    cd forexcurr

### Install
    npm install

---

### Start in Develop
    npm start

### Start in Production
    //change "homepage" in package.json
    "homepage": "http://localhost",
   
    //build
    npm run build

    //use static server
    serve -s build -p 5555

    //open in browser with address 
    "http://localhost:5555"

### Build & Start in Docker
    //change "homepage" in package.json
    "homepage": "http://localhost",

    //build
    docker build -t forexcurr .
   
    //start
    docker run -p 9090:5000 -d forexcurr
   
    //open in browser with address 
    "http://docker_ip:9090"  

#### Demo in GitLab
##### Link : [forexcurr](https://fidelstu.gitlab.io/forexcurr)